﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DB_Init3
{
    public class ContextInitializer : IDatabaseInitializer<CodeContext> 
    {
        public void InitializeDatabase(CodeContext db)
        {
            if (db.Database.Exists()) db.Database.Delete();
            db.Database.Create();
            db.Phones.AddRange(new[] { new Phone { Name = "LG Nexus 5", Price = 2000 }, new Phone { Name = "OnePlus 8T", Price = 8700 } });
            db.SaveChanges();
        }
         
    }
}
