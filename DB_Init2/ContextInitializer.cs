﻿using System.Data.Entity;
using Common;

namespace DB_Init2
{
    public class ContextInitializer : DropCreateDatabaseAlways<CodeContext> 
    {
        protected override void Seed(CodeContext db)
        {
            db.Phones.AddRange(new[] { new Phone { Name = "LG Nexus 5", Price = 5000 }, new Phone { Name = "Samsung Galaxy J5", Price = 4500 } });
            db.SaveChanges();
        }
    }
}