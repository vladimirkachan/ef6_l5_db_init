﻿using System;
using System.Data.Entity;
using System.Linq;
using Common;

namespace DB_Init2
{
    public class CodeContext : DbContext
    {
        static CodeContext()
        {
            Database.SetInitializer(new ContextInitializer());
        }
        public CodeContext()
            : base("name=CodeContext")
        {
        }
        public virtual DbSet<Phone> Phones { get; set; }
    }
}