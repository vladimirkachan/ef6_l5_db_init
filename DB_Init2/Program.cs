﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DB_Init2
{
    class Program
    {
        static void Main(string[] args)
        {
            using CodeContext db = new();
            db.Phones.Add(new Phone { Name = "Xiaomi Redmi Note 10 Pro", Price = 20000 });
            db.SaveChanges();
            foreach(var p in db.Phones) Console.WriteLine(p);
            Console.WriteLine($"\t{db.Phones.Count()}");
        }
    }
}
