﻿using System;
using System.Data.Entity;
using System.Linq;
using Common;

namespace DB_Init1
{
    public class CodeContext : DbContext
    {
        static CodeContext()
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists<CodeContext>());
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<CodeContext>());
            Database.SetInitializer(new DropCreateDatabaseAlways<CodeContext>());
        }
        public CodeContext()
            : base("name=CodeContext")
        {
        }

        public virtual DbSet<Phone> Phones { get; set; }
    }
}