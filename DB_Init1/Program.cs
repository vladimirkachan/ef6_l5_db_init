﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DB_Init1
{
    class Program
    {
        static void Main(string[] args)
        {
            using CodeContext db = new();
            db.Phones.AddRange(new[] { new Phone { Name = "LG Nexus 5", Price = 3000 }, new Phone { Name = "Samsung Galaxy J5", Price = 4000 } });
            db.SaveChanges();
            var q = from p in db.Phones select p; 
            foreach(var i in q) Console.WriteLine(i);
            Console.WriteLine($"\tcount = {q.Count()}");
            //Console.ReadKey();
        }
    }
}
